<?php
include('connection.php');
include('header.php');
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

//joining all the tables except teachers table

$sql = "select name,phone,email,address,class,subject,userid from users t1 inner join students t2 on t1.id = t2.userid inner join class t3 on t2.id = t3.studentid inner join subjects t4 on t2.id = t3.studentid"; 
$exe = $conn->query($sql);

// showing all the data
?>
<div class="row my-5">
  <div class="col-12 text-center">
    <h3>Student Detail</h3>
  </div>
</div>
<table border="2" class="table table-dark">
  <thead>
    <tr>
      <th>User ID</th>
      <th>Name</th>
      <th>phone</th>
      <th>Email</th>
      <th>Address</th>
      <th>Class</th>
      <th>Sunject</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php
     
        while( $row = $exe->fetch_assoc()){
        	?>
        	<tr>
        		<td><?php echo $row['userid'] ?></td>
        		<td><?php echo $row['name'] ?></td>
        		<td><?php echo $row['phone'] ?></td>
        		<td><?php echo $row['email'] ?></td>
        		<td><?php echo $row['address'] ?></td>
        		<td><?php echo $row['class'] ?></td>
        		<td><?php echo $row['subject'] ?></td>
        		<td><a href="/project/PHP/mysql/editstudent.php?id=<?php echo $row['userid'] ?>">Edit</a><a href="/project/PHP/mysql/deleteuser.php?id=<?php echo $row['userid'] ?>">Delete</a></td>
        	</tr>
       <?php
      }
    ?>
  </tbody>
</table>
<?php
include('footer.php');
?>