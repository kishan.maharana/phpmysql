<?php
include ('connection.php');
include('header.php');
// Check connection
if ($conn->connect_error)
{
    die("Connection failed: " . $conn->connect_error);
}

//joining teachers table with users table
$sql = "select userid,name,phone,email,address from users t1 inner join teachers t2 on t1.id = t2.userid";
$exe = $conn->query($sql);

// showing all the data

?>
<div class="row my-5">
  <div class="col-12 text-center">
    <h3>Teacher Detail</h3>
  </div>
</div>
<table border="2" class="table table-dark">
  <thead>
    <tr>
       <th>User ID</th>
      <th>Name</th>
      <th>phone</th>
      <th>Email</th>
      <th>Address</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php
while ($row = $exe->fetch_assoc())
{
?>
          <tr>
            <td><?php echo $row['userid'] ?></td>
            <td><?php echo $row['name'] ?></td>
            <td><?php echo $row['phone'] ?></td>
            <td><?php echo $row['email'] ?></td>
            <td><?php echo $row['address'] ?></td>
            
            <td><a href="/project/PHP/mysql/editteacher.php?id=<?php echo $row['userid'] ?>">Edit</a><a href="/project/PHP/mysql/deleteteacher.php?id=<?php echo $row['userid'] ?>">Delete</a></td>
          </tr>
       <?php
}
?>
  </tbody>
</table>
<?php
include('footer.php');
?>